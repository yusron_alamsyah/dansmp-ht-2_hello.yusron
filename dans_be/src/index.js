// const functions = require('@google-cloud/functions-framework');
const axios = require('axios');
const express = require("express");
const cors = require('cors');
const app = express();
const port = 5000

app.use(cors());


const usersRouter = require ('./controllers/users');
const jobRouter = require ('./controllers/jobs');

app.use(express.json());

app.use('/users', usersRouter);
app.use('/job', jobRouter);

app.get("/", (req, res) => {
    return res.status(200).json({ message: 'Hello World from route' })
})


app.listen(port, () => {
    console.log(` listening on port ${port}`)
})