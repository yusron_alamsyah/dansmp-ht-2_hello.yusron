const pool = require('../database/db');

const getUserByUsername = async (username) => {
  const [rows] = await pool.query('SELECT * FROM user WHERE username = ?', [username]);
  return rows[0] || null;
};

module.exports = {
  getUserByUsername
};