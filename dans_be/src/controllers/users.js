const express = require("express");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const router = express.Router();

const {getUserByUsername} = require("../models/userModel")

router.post('/login', async (req, res) => {
  const { username, password } = req.body;

  if (!username || !password) {
    return res.status(400).json({ error: 'Please provide both username and password.' });
  }

  const user = await getUserByUsername(username);

  if (!user) {
    return res.status(401).json({ error: 'Please check both username and password.' });
  }
  const passwordMatch = await bcrypt.compare(password, user.password);
  if (!passwordMatch) {
    return res.status(401).json({ error: 'Please check both username and password.' });
  }
  
  const token = jwt.sign({ userId: user.id }, process.env.JWT_SECRET, {
    expiresIn: '24h',
  });

  
  // const password = await bcrypt.hash("12345678", 12)
  res.status(200).json({ token });

});



const usersRouter = router;
module.exports = usersRouter;
