const express = require("express");
const axios = require("axios");
const auth = require('../auth/authenticate');

const router = express.Router();

router.get('/list',auth,async (req, res) => {
  try {

    const {description,location,full_time,current_page} = req.query 

    const page = parseInt(current_page)  || 1;
    const limit = 10;
    const offset = limit * page;

    const getAllJob =  await axios.get('https://dev6.dansmultipro.com/api/recruitment/positions.json') 
    const totalRows = getAllJob.data.length
    const totalPage = Math.ceil(totalRows / limit);
    
    const getJob = await axios.get('https://dev6.dansmultipro.com/api/recruitment/positions.json', {
      params: {
        page: page,
        description: description && description != null ? description : null,
        location: location && description != null ? location : null,
        full_time: full_time && description != null ? full_time : null,
      }
    })

    const isNull = getJob.data.some(obj => obj === null);
    const nextPage = page == totalPage || isNull  ? 0 : page +1

    res.status(200).send({
      data : getJob.data,
      page,
      totalPage,
      nextPage,
      totalRows
    });

  }catch (error) {
    res.status(500).json({ error: "Error, Something went wrong" });
  }
  
});

router.get('/:id',auth, async (req, res) => {
  try {
    const paramsId = req.params.id;
    const getJob = await axios.get(`https://dev6.dansmultipro.com/api/recruitment/positions/${paramsId}`)
    res.status(200).send({
      data : getJob.data
    });
  } catch (error) {
    res.status(500).json({ error: "Error, Something went wrong" });
  }
});

const jobsRouter = router;
module.exports = jobsRouter;
