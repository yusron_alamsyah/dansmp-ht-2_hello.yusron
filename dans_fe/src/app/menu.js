import { Navbar, Container } from 'react-bootstrap';

const CustomNavbar = () => {
  return (
    <Navbar className="bg-body-tertiary mb-5" bg="dark" data-bs-theme="dark">
      <Container>
        <Navbar.Brand href="/jobs"><b>Github </b> Job </Navbar.Brand>
      </Container>
    </Navbar>
  );
};

export default CustomNavbar;