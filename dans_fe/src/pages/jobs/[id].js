import React, { useState, useEffect } from 'react';
import RootLayout from '../../app/Layout';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { Button, Alert, Container, Row, Col, Image, Card, Stack } from 'react-bootstrap';
import { ST } from 'next/dist/shared/lib/utils';
import Navbar from '../../app/menu';



function DataList() {
  const [dataList, setDataList] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const router = useRouter();
  const { id } = router.query;

  const handleImageError = (event) => {
    event.target.src = 'https://demofree.sirv.com/nope-not-here.jpg'; // Replace with the path to your fallback image
  };

  const fetchData = async (id) => {
    setLoading(true);
    try {
      const jwtToken = localStorage.getItem('jwtToken');
      const headers = {
        'Authorization': `Bearer ${jwtToken}`
      }
      const response = await fetch(
        `http://localhost:5000/job/${id}`, { headers }

      );

      
      if(response.status == 401){
        router.push('/login');
      } 

      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }
      const data = await response.json();
      setDataList(data.data);
    } catch (err) {
      setError(err.message);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchData(id);
  }, [id]);

  return (
    <main>
    <Navbar />
      <Container>
        <Link href="/jobs" >
          <Button className='mb-3' variant="outline-primary">Back</Button>
        </Link>

        {loading ? (
          <p>Loading data...</p>
        ) : error ? (
          <p>Error: {error}</p>
        ) : (
          dataList &&
          <Card className="mb-2">
            <Card.Body>
              <Card.Title className='text-primary'>{dataList.title}</Card.Title>
              <Card.Text className='text-secondary'>
                {dataList.type} / {dataList.location}
              </Card.Text>
              <hr />
              <Row>
                <Col md={8}>
                  <Card.Text dangerouslySetInnerHTML={{ __html: dataList.description }}>
                  </Card.Text>
                </Col>
                <Col md={4}>
                  <Stack gap={2}>
                    <Alert variant="secondary">
                      <h5>{dataList.company}</h5>
                      <hr></hr>
                      <Image src={dataList.company_logo} onError={handleImageError}></Image>
                      <a href={dataList.company_url}>{dataList.company_url}</a>
                    </Alert>
                    <Alert variant="warning">
                      <h5>How to Apply</h5>
                      <hr></hr>
                      <div dangerouslySetInnerHTML={{ __html: dataList.how_to_apply }}></div>
                    </Alert>
                  </Stack>
                </Col>
              </Row>
            </Card.Body>
          </Card>
        )}
      </Container>
    </main>
  );
}

export default DataList;