import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

import RootLayout from '../app/Layout';

import Navbar from '../app/menu';

import { Button, ButtonGroup, Container, Row, Col, Form, Card,Stack } from 'react-bootstrap';

function DataList() {
  const router = useRouter();
  const [dataList, setDataList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [page, setPage] = useState(1);
  const [nextpage, setNextpage] = useState(1);


  const [desc, setDesc] = useState('');
  const [loc, setLoc] = useState('');
  const [full, setFull] = useState(false);

  const handleDescChange = (e) => setDesc(e.target.value);
  const handleLocChange = (e) => setLoc(e.target.value);

  const handleFull = () => {
    setFull(!full);
  };



  useEffect(() => {
    fetchData(1, '', '', '');
  }, []);

  const fetchData = async (page, desc, loc, full) => {
    setLoading(true);
    try {
      const jwtToken = localStorage.getItem('jwtToken');
      const headers = {
        'Authorization': `Bearer ${jwtToken}`
      }
      const response = await fetch(
        `http://localhost:5000/job/list?current_page=${page}&description=${desc}&location=${loc}&full_time=${full}`, { headers }

      );
      
      if(response.status == 401){
        router.push('/login');
      } 

      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }
      const data = await response.json();
      setDataList(data.data);
      setPage(data.page)
      setNextpage(data.nextPage)

    } catch (err) {
      setError(err.message);
    } finally {
      setLoading(false);
    }
  };

  const handleNext = () => {
    fetchData(nextpage, desc ? desc : '', loc ? loc : '', full ? full : ''); // Fetch data when the button is clicked
  };


  const handlePrevious = () => {
    fetchData(page - 1, desc ? desc : '', loc ? loc : '', full ? full : ''); // Fetch data when the button is clicked
  };

  const handleSearch = () => {
    fetchData(page, desc ? desc : '', loc ? loc : '', full ? full : ''); // Fetch data when the button is clicked
  };



  return (
    <main>
      <Navbar />
      <Container>
        <Row className="mb-3">
          <Col>
            <Form.Control
              type="text" value={desc} onChange={handleDescChange} placeholder="Search Job Description"
              aria-label="Description"
              aria-describedby="basic-addon1"
            />
          </Col>
          <Col>
            <Form.Control
              type="text" value={loc} onChange={handleLocChange} placeholder="Search Job Location"
              aria-label="Description"
              aria-describedby="basic-addon1"
            />
          </Col>
          <Col>
            <Form.Check // prettier-ignore
              type="switch"
              onChange={handleFull}
              name="full_time"
              checked={full}
              id="custom-switch"
              label="Full Time Only"
            />
            {full}
          </Col>
          <Col>
            <Button onClick={handleSearch} variant="outline-primary">Search</Button>
          </Col>
        </Row>


        {loading ? (
          <p>Loading data...</p>
        ) : error ? (
          <p>Error: {error}</p>
        ) : (
          <Stack gap={2} className="col-md-12 mx-auto">
            {dataList.map((item) => (
              item && (

                <Card className="mb-2">
                  <Card.Body>
                    <Card.Title className='text-primary'>{item.title}</Card.Title>
                    <Card.Text>
                      {item.company} - <b className='text-success font-weight-bold'>{item.type}</b>
                    </Card.Text>
                    <Link href="/jobs/[id]" as={`/jobs/${item.id}`}>
                      <Button variant="primary" size="sm">Detail</Button>
                    </Link>
                  </Card.Body>
                </Card>
              )
            ))}
          </Stack>
        )}

        <ButtonGroup className="mt-3 mb-3 float-right" aria-label="Pagination">
          <Button disabled={page == 1} onClick={handlePrevious} size="sm" variant="primary">Previous</Button>
          <Button disabled={nextpage == 0} onClick={handleNext} size="sm" variant="primary">Next</Button>
        </ButtonGroup>
      </Container>
    </main>
  );
}

export default DataList;