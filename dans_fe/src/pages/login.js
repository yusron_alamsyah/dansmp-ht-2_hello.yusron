import { useState } from 'react';
import { useRouter } from 'next/router';
import Swal from 'sweetalert2'
import { Button, ButtonGroup, Container, Row, Col, Form, Card, Stack } from 'react-bootstrap';
import RootLayout from '../app/Layout';


const LoginForm = () => {
    const router = useRouter();

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const handleUsernameChange = (e) => setUsername(e.target.value);
    const handlePasswordChange = (e) => setPassword(e.target.value);

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await fetch('http://localhost:5000/users/login', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ username, password }),
            });
            const result = await response.json();

            if (response.ok) {
                // Swal.fire({
                //     title: 'Success',
                //     text: 'you have successfully logged in',
                //     icon: 'success',
                //     timer: 1000
                // })
                localStorage.setItem('jwtToken', result.token);
                router.push('/jobs'); // Redirect to dashboard upon successful login
            } else {
                Swal.fire({
                    title: 'Login failed',
                    text: result.error,
                    icon: 'warning',
                })
            }
        } catch (error) {
            Swal.fire({
                title: 'Login failed',
                text: "Something went wrong",
                icon: 'error',
            })
        }
    };

    return (
        <main>
            {/* <Container> */}
                <div className="d-flex col-md-12 justify-content-center align-items-center" style={{ minHeight: '100vh' }}>
                    <div className='col-md-4'>
                    <Form onSubmit={handleSubmit}>
                        <h3>Login Page</h3>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Username</Form.Label>
                            <Form.Control type="text" value={username} onChange={handleUsernameChange} placeholder="Enter Username" />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" value={password} onChange={handlePasswordChange} placeholder="Enter Password" />
                        </Form.Group>
                        <div className="d-grid gap-2">

                        <Button variant="primary" type="submit">
                        Login
                        </Button>
                        </div>
                    </Form>
                    </div>
                </div>
            {/* </Container> */}
        </main>
    );
};

export default LoginForm;