import { useRouter } from 'next/router';
import { useEffect } from 'react';

const IndexPage = () => {
  const router = useRouter();

  useEffect(() => {
    // Check if JWT token exists in local storage
    const token = localStorage.getItem('jwtToken');
    if (!token) {
      // Redirect to the login page
      router.push('/login');
    }else{
      router.push('/jobs');
    }
  }, [router]);

  return (
    <div>
      <p>Redirect......</p>
    </div>
  );
};

export default IndexPage;