/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/jobs";
exports.ids = ["pages/jobs"];
exports.modules = {

/***/ "./node_modules/next/dist/build/webpack/loaders/next-route-loader/index.js?kind=PAGES&page=%2Fjobs&preferredRegion=&absolutePagePath=.%2Fsrc%5Cpages%5Cjobs.js&absoluteAppPath=private-next-pages%2F_app&absoluteDocumentPath=private-next-pages%2F_document&middlewareConfigBase64=e30%3D!":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/next/dist/build/webpack/loaders/next-route-loader/index.js?kind=PAGES&page=%2Fjobs&preferredRegion=&absolutePagePath=.%2Fsrc%5Cpages%5Cjobs.js&absoluteAppPath=private-next-pages%2F_app&absoluteDocumentPath=private-next-pages%2F_document&middlewareConfigBase64=e30%3D! ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   config: () => (/* binding */ config),\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__),\n/* harmony export */   getServerSideProps: () => (/* binding */ getServerSideProps),\n/* harmony export */   getStaticPaths: () => (/* binding */ getStaticPaths),\n/* harmony export */   getStaticProps: () => (/* binding */ getStaticProps),\n/* harmony export */   reportWebVitals: () => (/* binding */ reportWebVitals),\n/* harmony export */   routeModule: () => (/* binding */ routeModule),\n/* harmony export */   unstable_getServerProps: () => (/* binding */ unstable_getServerProps),\n/* harmony export */   unstable_getServerSideProps: () => (/* binding */ unstable_getServerSideProps),\n/* harmony export */   unstable_getStaticParams: () => (/* binding */ unstable_getStaticParams),\n/* harmony export */   unstable_getStaticPaths: () => (/* binding */ unstable_getStaticPaths),\n/* harmony export */   unstable_getStaticProps: () => (/* binding */ unstable_getStaticProps)\n/* harmony export */ });\n/* harmony import */ var next_dist_server_future_route_modules_pages_module_compiled__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! next/dist/server/future/route-modules/pages/module.compiled */ \"./node_modules/next/dist/server/future/route-modules/pages/module.compiled.js\");\n/* harmony import */ var next_dist_server_future_route_modules_pages_module_compiled__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_dist_server_future_route_modules_pages_module_compiled__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var next_dist_server_future_route_kind__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/dist/server/future/route-kind */ \"./node_modules/next/dist/server/future/route-kind.js\");\n/* harmony import */ var next_dist_build_templates_helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/dist/build/templates/helpers */ \"./node_modules/next/dist/build/templates/helpers.js\");\n/* harmony import */ var private_next_pages_document__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! private-next-pages/_document */ \"./node_modules/next/dist/pages/_document.js\");\n/* harmony import */ var private_next_pages_document__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(private_next_pages_document__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var private_next_pages_app__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! private-next-pages/_app */ \"./node_modules/next/dist/pages/_app.js\");\n/* harmony import */ var private_next_pages_app__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(private_next_pages_app__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _src_pages_jobs_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./src\\pages\\jobs.js */ \"./src/pages/jobs.js\");\n\n\n\n// Import the app and document modules.\n\n\n// Import the userland code.\n\n// Re-export the component (should be the default export).\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,next_dist_build_templates_helpers__WEBPACK_IMPORTED_MODULE_2__.hoist)(_src_pages_jobs_js__WEBPACK_IMPORTED_MODULE_5__, \"default\"));\n// Re-export methods.\nconst getStaticProps = (0,next_dist_build_templates_helpers__WEBPACK_IMPORTED_MODULE_2__.hoist)(_src_pages_jobs_js__WEBPACK_IMPORTED_MODULE_5__, \"getStaticProps\");\nconst getStaticPaths = (0,next_dist_build_templates_helpers__WEBPACK_IMPORTED_MODULE_2__.hoist)(_src_pages_jobs_js__WEBPACK_IMPORTED_MODULE_5__, \"getStaticPaths\");\nconst getServerSideProps = (0,next_dist_build_templates_helpers__WEBPACK_IMPORTED_MODULE_2__.hoist)(_src_pages_jobs_js__WEBPACK_IMPORTED_MODULE_5__, \"getServerSideProps\");\nconst config = (0,next_dist_build_templates_helpers__WEBPACK_IMPORTED_MODULE_2__.hoist)(_src_pages_jobs_js__WEBPACK_IMPORTED_MODULE_5__, \"config\");\nconst reportWebVitals = (0,next_dist_build_templates_helpers__WEBPACK_IMPORTED_MODULE_2__.hoist)(_src_pages_jobs_js__WEBPACK_IMPORTED_MODULE_5__, \"reportWebVitals\");\n// Re-export legacy methods.\nconst unstable_getStaticProps = (0,next_dist_build_templates_helpers__WEBPACK_IMPORTED_MODULE_2__.hoist)(_src_pages_jobs_js__WEBPACK_IMPORTED_MODULE_5__, \"unstable_getStaticProps\");\nconst unstable_getStaticPaths = (0,next_dist_build_templates_helpers__WEBPACK_IMPORTED_MODULE_2__.hoist)(_src_pages_jobs_js__WEBPACK_IMPORTED_MODULE_5__, \"unstable_getStaticPaths\");\nconst unstable_getStaticParams = (0,next_dist_build_templates_helpers__WEBPACK_IMPORTED_MODULE_2__.hoist)(_src_pages_jobs_js__WEBPACK_IMPORTED_MODULE_5__, \"unstable_getStaticParams\");\nconst unstable_getServerProps = (0,next_dist_build_templates_helpers__WEBPACK_IMPORTED_MODULE_2__.hoist)(_src_pages_jobs_js__WEBPACK_IMPORTED_MODULE_5__, \"unstable_getServerProps\");\nconst unstable_getServerSideProps = (0,next_dist_build_templates_helpers__WEBPACK_IMPORTED_MODULE_2__.hoist)(_src_pages_jobs_js__WEBPACK_IMPORTED_MODULE_5__, \"unstable_getServerSideProps\");\n// Create and export the route module that will be consumed.\nconst routeModule = new next_dist_server_future_route_modules_pages_module_compiled__WEBPACK_IMPORTED_MODULE_0__.PagesRouteModule({\n    definition: {\n        kind: next_dist_server_future_route_kind__WEBPACK_IMPORTED_MODULE_1__.RouteKind.PAGES,\n        page: \"/jobs\",\n        pathname: \"/jobs\",\n        // The following aren't used in production.\n        bundlePath: \"\",\n        filename: \"\"\n    },\n    components: {\n        App: (private_next_pages_app__WEBPACK_IMPORTED_MODULE_4___default()),\n        Document: (private_next_pages_document__WEBPACK_IMPORTED_MODULE_3___default())\n    },\n    userland: _src_pages_jobs_js__WEBPACK_IMPORTED_MODULE_5__\n});\n\n//# sourceMappingURL=pages.js.map//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvbmV4dC9kaXN0L2J1aWxkL3dlYnBhY2svbG9hZGVycy9uZXh0LXJvdXRlLWxvYWRlci9pbmRleC5qcz9raW5kPVBBR0VTJnBhZ2U9JTJGam9icyZwcmVmZXJyZWRSZWdpb249JmFic29sdXRlUGFnZVBhdGg9LiUyRnNyYyU1Q3BhZ2VzJTVDam9icy5qcyZhYnNvbHV0ZUFwcFBhdGg9cHJpdmF0ZS1uZXh0LXBhZ2VzJTJGX2FwcCZhYnNvbHV0ZURvY3VtZW50UGF0aD1wcml2YXRlLW5leHQtcGFnZXMlMkZfZG9jdW1lbnQmbWlkZGxld2FyZUNvbmZpZ0Jhc2U2ND1lMzAlM0QhIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUErRjtBQUNoQztBQUNMO0FBQzFEO0FBQ29EO0FBQ1Y7QUFDMUM7QUFDa0Q7QUFDbEQ7QUFDQSxpRUFBZSx3RUFBSyxDQUFDLCtDQUFRLFlBQVksRUFBQztBQUMxQztBQUNPLHVCQUF1Qix3RUFBSyxDQUFDLCtDQUFRO0FBQ3JDLHVCQUF1Qix3RUFBSyxDQUFDLCtDQUFRO0FBQ3JDLDJCQUEyQix3RUFBSyxDQUFDLCtDQUFRO0FBQ3pDLGVBQWUsd0VBQUssQ0FBQywrQ0FBUTtBQUM3Qix3QkFBd0Isd0VBQUssQ0FBQywrQ0FBUTtBQUM3QztBQUNPLGdDQUFnQyx3RUFBSyxDQUFDLCtDQUFRO0FBQzlDLGdDQUFnQyx3RUFBSyxDQUFDLCtDQUFRO0FBQzlDLGlDQUFpQyx3RUFBSyxDQUFDLCtDQUFRO0FBQy9DLGdDQUFnQyx3RUFBSyxDQUFDLCtDQUFRO0FBQzlDLG9DQUFvQyx3RUFBSyxDQUFDLCtDQUFRO0FBQ3pEO0FBQ08sd0JBQXdCLHlHQUFnQjtBQUMvQztBQUNBLGNBQWMseUVBQVM7QUFDdkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLFdBQVc7QUFDWCxnQkFBZ0I7QUFDaEIsS0FBSztBQUNMLFlBQVk7QUFDWixDQUFDOztBQUVEIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vZGFuc19mZS8/Yzc1ZiJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQYWdlc1JvdXRlTW9kdWxlIH0gZnJvbSBcIm5leHQvZGlzdC9zZXJ2ZXIvZnV0dXJlL3JvdXRlLW1vZHVsZXMvcGFnZXMvbW9kdWxlLmNvbXBpbGVkXCI7XG5pbXBvcnQgeyBSb3V0ZUtpbmQgfSBmcm9tIFwibmV4dC9kaXN0L3NlcnZlci9mdXR1cmUvcm91dGUta2luZFwiO1xuaW1wb3J0IHsgaG9pc3QgfSBmcm9tIFwibmV4dC9kaXN0L2J1aWxkL3RlbXBsYXRlcy9oZWxwZXJzXCI7XG4vLyBJbXBvcnQgdGhlIGFwcCBhbmQgZG9jdW1lbnQgbW9kdWxlcy5cbmltcG9ydCBEb2N1bWVudCBmcm9tIFwicHJpdmF0ZS1uZXh0LXBhZ2VzL19kb2N1bWVudFwiO1xuaW1wb3J0IEFwcCBmcm9tIFwicHJpdmF0ZS1uZXh0LXBhZ2VzL19hcHBcIjtcbi8vIEltcG9ydCB0aGUgdXNlcmxhbmQgY29kZS5cbmltcG9ydCAqIGFzIHVzZXJsYW5kIGZyb20gXCIuL3NyY1xcXFxwYWdlc1xcXFxqb2JzLmpzXCI7XG4vLyBSZS1leHBvcnQgdGhlIGNvbXBvbmVudCAoc2hvdWxkIGJlIHRoZSBkZWZhdWx0IGV4cG9ydCkuXG5leHBvcnQgZGVmYXVsdCBob2lzdCh1c2VybGFuZCwgXCJkZWZhdWx0XCIpO1xuLy8gUmUtZXhwb3J0IG1ldGhvZHMuXG5leHBvcnQgY29uc3QgZ2V0U3RhdGljUHJvcHMgPSBob2lzdCh1c2VybGFuZCwgXCJnZXRTdGF0aWNQcm9wc1wiKTtcbmV4cG9ydCBjb25zdCBnZXRTdGF0aWNQYXRocyA9IGhvaXN0KHVzZXJsYW5kLCBcImdldFN0YXRpY1BhdGhzXCIpO1xuZXhwb3J0IGNvbnN0IGdldFNlcnZlclNpZGVQcm9wcyA9IGhvaXN0KHVzZXJsYW5kLCBcImdldFNlcnZlclNpZGVQcm9wc1wiKTtcbmV4cG9ydCBjb25zdCBjb25maWcgPSBob2lzdCh1c2VybGFuZCwgXCJjb25maWdcIik7XG5leHBvcnQgY29uc3QgcmVwb3J0V2ViVml0YWxzID0gaG9pc3QodXNlcmxhbmQsIFwicmVwb3J0V2ViVml0YWxzXCIpO1xuLy8gUmUtZXhwb3J0IGxlZ2FjeSBtZXRob2RzLlxuZXhwb3J0IGNvbnN0IHVuc3RhYmxlX2dldFN0YXRpY1Byb3BzID0gaG9pc3QodXNlcmxhbmQsIFwidW5zdGFibGVfZ2V0U3RhdGljUHJvcHNcIik7XG5leHBvcnQgY29uc3QgdW5zdGFibGVfZ2V0U3RhdGljUGF0aHMgPSBob2lzdCh1c2VybGFuZCwgXCJ1bnN0YWJsZV9nZXRTdGF0aWNQYXRoc1wiKTtcbmV4cG9ydCBjb25zdCB1bnN0YWJsZV9nZXRTdGF0aWNQYXJhbXMgPSBob2lzdCh1c2VybGFuZCwgXCJ1bnN0YWJsZV9nZXRTdGF0aWNQYXJhbXNcIik7XG5leHBvcnQgY29uc3QgdW5zdGFibGVfZ2V0U2VydmVyUHJvcHMgPSBob2lzdCh1c2VybGFuZCwgXCJ1bnN0YWJsZV9nZXRTZXJ2ZXJQcm9wc1wiKTtcbmV4cG9ydCBjb25zdCB1bnN0YWJsZV9nZXRTZXJ2ZXJTaWRlUHJvcHMgPSBob2lzdCh1c2VybGFuZCwgXCJ1bnN0YWJsZV9nZXRTZXJ2ZXJTaWRlUHJvcHNcIik7XG4vLyBDcmVhdGUgYW5kIGV4cG9ydCB0aGUgcm91dGUgbW9kdWxlIHRoYXQgd2lsbCBiZSBjb25zdW1lZC5cbmV4cG9ydCBjb25zdCByb3V0ZU1vZHVsZSA9IG5ldyBQYWdlc1JvdXRlTW9kdWxlKHtcbiAgICBkZWZpbml0aW9uOiB7XG4gICAgICAgIGtpbmQ6IFJvdXRlS2luZC5QQUdFUyxcbiAgICAgICAgcGFnZTogXCIvam9ic1wiLFxuICAgICAgICBwYXRobmFtZTogXCIvam9ic1wiLFxuICAgICAgICAvLyBUaGUgZm9sbG93aW5nIGFyZW4ndCB1c2VkIGluIHByb2R1Y3Rpb24uXG4gICAgICAgIGJ1bmRsZVBhdGg6IFwiXCIsXG4gICAgICAgIGZpbGVuYW1lOiBcIlwiXG4gICAgfSxcbiAgICBjb21wb25lbnRzOiB7XG4gICAgICAgIEFwcCxcbiAgICAgICAgRG9jdW1lbnRcbiAgICB9LFxuICAgIHVzZXJsYW5kXG59KTtcblxuLy8jIHNvdXJjZU1hcHBpbmdVUkw9cGFnZXMuanMubWFwIl0sIm5hbWVzIjpbXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./node_modules/next/dist/build/webpack/loaders/next-route-loader/index.js?kind=PAGES&page=%2Fjobs&preferredRegion=&absolutePagePath=.%2Fsrc%5Cpages%5Cjobs.js&absoluteAppPath=private-next-pages%2F_app&absoluteDocumentPath=private-next-pages%2F_document&middlewareConfigBase64=e30%3D!\n");

/***/ }),

/***/ "./src/app/Layout.tsx":
/*!****************************!*\
  !*** ./src/app/Layout.tsx ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ RootLayout),\n/* harmony export */   metadata: () => (/* binding */ metadata)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var next_font_google_target_css_path_src_app_Layout_tsx_import_Inter_arguments_subsets_latin_variableName_inter___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/font/google/target.css?{\"path\":\"src\\\\app\\\\Layout.tsx\",\"import\":\"Inter\",\"arguments\":[{\"subsets\":[\"latin\"]}],\"variableName\":\"inter\"} */ \"./node_modules/next/font/google/target.css?{\\\"path\\\":\\\"src\\\\\\\\app\\\\\\\\Layout.tsx\\\",\\\"import\\\":\\\"Inter\\\",\\\"arguments\\\":[{\\\"subsets\\\":[\\\"latin\\\"]}],\\\"variableName\\\":\\\"inter\\\"}\");\n/* harmony import */ var next_font_google_target_css_path_src_app_Layout_tsx_import_Inter_arguments_subsets_latin_variableName_inter___WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_font_google_target_css_path_src_app_Layout_tsx_import_Inter_arguments_subsets_latin_variableName_inter___WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _globals_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./globals.css */ \"./src/app/globals.css\");\n/* harmony import */ var _globals_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_globals_css__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! bootstrap/dist/css/bootstrap.css */ \"./node_modules/bootstrap/dist/css/bootstrap.css\");\n/* harmony import */ var bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_2__);\n\n\n\n\nconst metadata = {\n    title: \"Dans Test\",\n    description: \"created by Yusron Alamsyah\"\n};\nfunction RootLayout({ children }) {\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"html\", {\n        lang: \"en\",\n        children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"body\", {\n            className: (next_font_google_target_css_path_src_app_Layout_tsx_import_Inter_arguments_subsets_latin_variableName_inter___WEBPACK_IMPORTED_MODULE_3___default().className),\n            children: children\n        }, void 0, false, {\n            fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\app\\\\Layout.tsx\",\n            lineNumber: 20,\n            columnNumber: 7\n        }, this)\n    }, void 0, false, {\n        fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\app\\\\Layout.tsx\",\n        lineNumber: 19,\n        columnNumber: 5\n    }, this);\n}\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL0xheW91dC50c3giLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7QUFLTUE7QUFIaUI7QUFDbUI7QUFJbkMsTUFBTUMsV0FBcUI7SUFDaENDLE9BQU87SUFDUEMsYUFBYTtBQUNmLEVBQUU7QUFFYSxTQUFTQyxXQUFXLEVBQ2pDQyxRQUFRLEVBR1I7SUFDQSxxQkFDRSw4REFBQ0M7UUFBS0MsTUFBSztrQkFDVCw0RUFBQ0M7WUFBS0MsV0FBV1QsK0pBQWU7c0JBQzdCSzs7Ozs7Ozs7Ozs7QUFJVCIsInNvdXJjZXMiOlsid2VicGFjazovL2RhbnNfZmUvLi9zcmMvYXBwL0xheW91dC50c3g/YmE0NCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgdHlwZSB7IE1ldGFkYXRhIH0gZnJvbSBcIm5leHRcIjtcbmltcG9ydCB7IEludGVyIH0gZnJvbSBcIm5leHQvZm9udC9nb29nbGVcIjtcbmltcG9ydCBcIi4vZ2xvYmFscy5jc3NcIjtcbmltcG9ydCAnYm9vdHN0cmFwL2Rpc3QvY3NzL2Jvb3RzdHJhcC5jc3MnO1xuXG5jb25zdCBpbnRlciA9IEludGVyKHsgc3Vic2V0czogW1wibGF0aW5cIl0gfSk7XG5cbmV4cG9ydCBjb25zdCBtZXRhZGF0YTogTWV0YWRhdGEgPSB7XG4gIHRpdGxlOiBcIkRhbnMgVGVzdFwiLFxuICBkZXNjcmlwdGlvbjogXCJjcmVhdGVkIGJ5IFl1c3JvbiBBbGFtc3lhaFwiLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gUm9vdExheW91dCh7XG4gIGNoaWxkcmVuLFxufTogUmVhZG9ubHk8e1xuICBjaGlsZHJlbjogUmVhY3QuUmVhY3ROb2RlO1xufT4pIHtcbiAgcmV0dXJuIChcbiAgICA8aHRtbCBsYW5nPVwiZW5cIj5cbiAgICAgIDxib2R5IGNsYXNzTmFtZT17aW50ZXIuY2xhc3NOYW1lfT5cbiAgICAgICAge2NoaWxkcmVufVxuICAgICAgPC9ib2R5PlxuICAgIDwvaHRtbD5cbiAgKTtcbn1cbiJdLCJuYW1lcyI6WyJpbnRlciIsIm1ldGFkYXRhIiwidGl0bGUiLCJkZXNjcmlwdGlvbiIsIlJvb3RMYXlvdXQiLCJjaGlsZHJlbiIsImh0bWwiLCJsYW5nIiwiYm9keSIsImNsYXNzTmFtZSJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/app/Layout.tsx\n");

/***/ }),

/***/ "./src/app/menu.js":
/*!*************************!*\
  !*** ./src/app/menu.js ***!
  \*************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _barrel_optimize_names_Container_Navbar_react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! __barrel_optimize__?names=Container,Navbar!=!react-bootstrap */ \"./node_modules/react-bootstrap/esm/Navbar.js\");\n/* harmony import */ var _barrel_optimize_names_Container_Navbar_react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! __barrel_optimize__?names=Container,Navbar!=!react-bootstrap */ \"./node_modules/react-bootstrap/esm/Container.js\");\n\n\nconst CustomNavbar = ()=>{\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Container_Navbar_react_bootstrap__WEBPACK_IMPORTED_MODULE_1__[\"default\"], {\n        className: \"bg-body-tertiary mb-5\",\n        bg: \"dark\",\n        \"data-bs-theme\": \"dark\",\n        children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Container_Navbar_react_bootstrap__WEBPACK_IMPORTED_MODULE_2__[\"default\"], {\n            children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Container_Navbar_react_bootstrap__WEBPACK_IMPORTED_MODULE_1__[\"default\"].Brand, {\n                href: \"/jobs\",\n                children: [\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"b\", {\n                        children: \"Github \"\n                    }, void 0, false, {\n                        fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\app\\\\menu.js\",\n                        lineNumber: 7,\n                        columnNumber: 36\n                    }, undefined),\n                    \" Job \"\n                ]\n            }, void 0, true, {\n                fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\app\\\\menu.js\",\n                lineNumber: 7,\n                columnNumber: 9\n            }, undefined)\n        }, void 0, false, {\n            fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\app\\\\menu.js\",\n            lineNumber: 6,\n            columnNumber: 7\n        }, undefined)\n    }, void 0, false, {\n        fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\app\\\\menu.js\",\n        lineNumber: 5,\n        columnNumber: 5\n    }, undefined);\n};\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CustomNavbar);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL21lbnUuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQW9EO0FBRXBELE1BQU1FLGVBQWU7SUFDbkIscUJBQ0UsOERBQUNGLCtGQUFNQTtRQUFDRyxXQUFVO1FBQXdCQyxJQUFHO1FBQU9DLGlCQUFjO2tCQUNoRSw0RUFBQ0osK0ZBQVNBO3NCQUNSLDRFQUFDRCwrRkFBTUEsQ0FBQ00sS0FBSztnQkFBQ0MsTUFBSzs7a0NBQVEsOERBQUNDO2tDQUFFOzs7Ozs7b0JBQVc7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBSWpEO0FBRUEsaUVBQWVOLFlBQVlBLEVBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9kYW5zX2ZlLy4vc3JjL2FwcC9tZW51LmpzP2U0ZmYiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmF2YmFyLCBDb250YWluZXIgfSBmcm9tICdyZWFjdC1ib290c3RyYXAnO1xyXG5cclxuY29uc3QgQ3VzdG9tTmF2YmFyID0gKCkgPT4ge1xyXG4gIHJldHVybiAoXHJcbiAgICA8TmF2YmFyIGNsYXNzTmFtZT1cImJnLWJvZHktdGVydGlhcnkgbWItNVwiIGJnPVwiZGFya1wiIGRhdGEtYnMtdGhlbWU9XCJkYXJrXCI+XHJcbiAgICAgIDxDb250YWluZXI+XHJcbiAgICAgICAgPE5hdmJhci5CcmFuZCBocmVmPVwiL2pvYnNcIj48Yj5HaXRodWIgPC9iPiBKb2IgPC9OYXZiYXIuQnJhbmQ+XHJcbiAgICAgIDwvQ29udGFpbmVyPlxyXG4gICAgPC9OYXZiYXI+XHJcbiAgKTtcclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IEN1c3RvbU5hdmJhcjsiXSwibmFtZXMiOlsiTmF2YmFyIiwiQ29udGFpbmVyIiwiQ3VzdG9tTmF2YmFyIiwiY2xhc3NOYW1lIiwiYmciLCJkYXRhLWJzLXRoZW1lIiwiQnJhbmQiLCJocmVmIiwiYiJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/app/menu.js\n");

/***/ }),

/***/ "./src/pages/jobs.js":
/*!***************************!*\
  !*** ./src/pages/jobs.js ***!
  \***************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/link */ \"./node_modules/next/link.js\");\n/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ \"./node_modules/next/router.js\");\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _app_Layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../app/Layout */ \"./src/app/Layout.tsx\");\n/* harmony import */ var _app_menu__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../app/menu */ \"./src/app/menu.js\");\n/* harmony import */ var _barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! __barrel_optimize__?names=Button,ButtonGroup,Card,Col,Container,Form,Row,Stack!=!react-bootstrap */ \"./node_modules/react-bootstrap/esm/Container.js\");\n/* harmony import */ var _barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! __barrel_optimize__?names=Button,ButtonGroup,Card,Col,Container,Form,Row,Stack!=!react-bootstrap */ \"./node_modules/react-bootstrap/esm/Row.js\");\n/* harmony import */ var _barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! __barrel_optimize__?names=Button,ButtonGroup,Card,Col,Container,Form,Row,Stack!=!react-bootstrap */ \"./node_modules/react-bootstrap/esm/Col.js\");\n/* harmony import */ var _barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! __barrel_optimize__?names=Button,ButtonGroup,Card,Col,Container,Form,Row,Stack!=!react-bootstrap */ \"./node_modules/react-bootstrap/esm/Form.js\");\n/* harmony import */ var _barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! __barrel_optimize__?names=Button,ButtonGroup,Card,Col,Container,Form,Row,Stack!=!react-bootstrap */ \"./node_modules/react-bootstrap/esm/Button.js\");\n/* harmony import */ var _barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! __barrel_optimize__?names=Button,ButtonGroup,Card,Col,Container,Form,Row,Stack!=!react-bootstrap */ \"./node_modules/react-bootstrap/esm/Stack.js\");\n/* harmony import */ var _barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! __barrel_optimize__?names=Button,ButtonGroup,Card,Col,Container,Form,Row,Stack!=!react-bootstrap */ \"./node_modules/react-bootstrap/esm/Card.js\");\n/* harmony import */ var _barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! __barrel_optimize__?names=Button,ButtonGroup,Card,Col,Container,Form,Row,Stack!=!react-bootstrap */ \"./node_modules/react-bootstrap/esm/ButtonGroup.js\");\n\n\n\n\n\n\n\nfunction DataList() {\n    const router = (0,next_router__WEBPACK_IMPORTED_MODULE_3__.useRouter)();\n    const [dataList, setDataList] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)([]);\n    const [loading, setLoading] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);\n    const [error, setError] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(null);\n    const [page, setPage] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(1);\n    const [nextpage, setNextpage] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(1);\n    const [desc, setDesc] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(\"\");\n    const [loc, setLoc] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(\"\");\n    const [full, setFull] = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);\n    const handleDescChange = (e)=>setDesc(e.target.value);\n    const handleLocChange = (e)=>setLoc(e.target.value);\n    const handleFull = ()=>{\n        setFull(!full);\n    };\n    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>{\n        fetchData(1, \"\", \"\", \"\");\n    }, []);\n    const fetchData = async (page, desc, loc, full)=>{\n        setLoading(true);\n        try {\n            const jwtToken = localStorage.getItem(\"jwtToken\");\n            const headers = {\n                \"Authorization\": `Bearer ${jwtToken}`\n            };\n            const response = await fetch(`http://localhost:5000/job/list?current_page=${page}&description=${desc}&location=${loc}&full_time=${full}`, {\n                headers\n            });\n            if (response.status == 401) {\n                router.push(\"/login\");\n            }\n            if (!response.ok) {\n                throw new Error(\"Failed to fetch data\");\n            }\n            const data = await response.json();\n            setDataList(data.data);\n            setPage(data.page);\n            setNextpage(data.nextPage);\n        } catch (err) {\n            setError(err.message);\n        } finally{\n            setLoading(false);\n        }\n    };\n    const handleNext = ()=>{\n        fetchData(nextpage, desc ? desc : \"\", loc ? loc : \"\", full ? full : \"\"); // Fetch data when the button is clicked\n    };\n    const handlePrevious = ()=>{\n        fetchData(page - 1, desc ? desc : \"\", loc ? loc : \"\", full ? full : \"\"); // Fetch data when the button is clicked\n    };\n    const handleSearch = ()=>{\n        fetchData(page, desc ? desc : \"\", loc ? loc : \"\", full ? full : \"\"); // Fetch data when the button is clicked\n    };\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"main\", {\n        children: [\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_app_menu__WEBPACK_IMPORTED_MODULE_5__[\"default\"], {}, void 0, false, {\n                fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                lineNumber: 85,\n                columnNumber: 7\n            }, this),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_6__[\"default\"], {\n                children: [\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_7__[\"default\"], {\n                        className: \"mb-3\",\n                        children: [\n                            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_8__[\"default\"], {\n                                children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_9__[\"default\"].Control, {\n                                    type: \"text\",\n                                    value: desc,\n                                    onChange: handleDescChange,\n                                    placeholder: \"Search Job Description\",\n                                    \"aria-label\": \"Description\",\n                                    \"aria-describedby\": \"basic-addon1\"\n                                }, void 0, false, {\n                                    fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                                    lineNumber: 89,\n                                    columnNumber: 13\n                                }, this)\n                            }, void 0, false, {\n                                fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                                lineNumber: 88,\n                                columnNumber: 11\n                            }, this),\n                            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_8__[\"default\"], {\n                                children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_9__[\"default\"].Control, {\n                                    type: \"text\",\n                                    value: loc,\n                                    onChange: handleLocChange,\n                                    placeholder: \"Search Job Location\",\n                                    \"aria-label\": \"Description\",\n                                    \"aria-describedby\": \"basic-addon1\"\n                                }, void 0, false, {\n                                    fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                                    lineNumber: 96,\n                                    columnNumber: 13\n                                }, this)\n                            }, void 0, false, {\n                                fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                                lineNumber: 95,\n                                columnNumber: 11\n                            }, this),\n                            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_8__[\"default\"], {\n                                children: [\n                                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_9__[\"default\"].Check // prettier-ignore\n                                    , {\n                                        type: \"switch\",\n                                        onChange: handleFull,\n                                        name: \"full_time\",\n                                        checked: full,\n                                        id: \"custom-switch\",\n                                        label: \"Full Time Only\"\n                                    }, void 0, false, {\n                                        fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                                        lineNumber: 103,\n                                        columnNumber: 13\n                                    }, this),\n                                    full\n                                ]\n                            }, void 0, true, {\n                                fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                                lineNumber: 102,\n                                columnNumber: 11\n                            }, this),\n                            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_8__[\"default\"], {\n                                children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_10__[\"default\"], {\n                                    onClick: handleSearch,\n                                    variant: \"outline-primary\",\n                                    children: \"Search\"\n                                }, void 0, false, {\n                                    fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                                    lineNumber: 114,\n                                    columnNumber: 13\n                                }, this)\n                            }, void 0, false, {\n                                fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                                lineNumber: 113,\n                                columnNumber: 11\n                            }, this)\n                        ]\n                    }, void 0, true, {\n                        fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                        lineNumber: 87,\n                        columnNumber: 9\n                    }, this),\n                    loading ? /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"p\", {\n                        children: \"Loading data...\"\n                    }, void 0, false, {\n                        fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                        lineNumber: 120,\n                        columnNumber: 11\n                    }, this) : error ? /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"p\", {\n                        children: [\n                            \"Error: \",\n                            error\n                        ]\n                    }, void 0, true, {\n                        fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                        lineNumber: 122,\n                        columnNumber: 11\n                    }, this) : /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_11__[\"default\"], {\n                        gap: 2,\n                        className: \"col-md-12 mx-auto\",\n                        children: dataList.map((item)=>item && /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_12__[\"default\"], {\n                                className: \"mb-2\",\n                                children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_12__[\"default\"].Body, {\n                                    children: [\n                                        /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_12__[\"default\"].Title, {\n                                            className: \"text-primary\",\n                                            children: item.title\n                                        }, void 0, false, {\n                                            fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                                            lineNumber: 130,\n                                            columnNumber: 21\n                                        }, this),\n                                        /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_12__[\"default\"].Text, {\n                                            children: [\n                                                item.company,\n                                                \" - \",\n                                                /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"b\", {\n                                                    className: \"text-success font-weight-bold\",\n                                                    children: item.type\n                                                }, void 0, false, {\n                                                    fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                                                    lineNumber: 132,\n                                                    columnNumber: 40\n                                                }, this)\n                                            ]\n                                        }, void 0, true, {\n                                            fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                                            lineNumber: 131,\n                                            columnNumber: 21\n                                        }, this),\n                                        /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((next_link__WEBPACK_IMPORTED_MODULE_2___default()), {\n                                            href: \"/jobs/[id]\",\n                                            as: `/jobs/${item.id}`,\n                                            children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_10__[\"default\"], {\n                                                variant: \"primary\",\n                                                size: \"sm\",\n                                                children: \"Detail\"\n                                            }, void 0, false, {\n                                                fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                                                lineNumber: 135,\n                                                columnNumber: 23\n                                            }, this)\n                                        }, void 0, false, {\n                                            fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                                            lineNumber: 134,\n                                            columnNumber: 21\n                                        }, this)\n                                    ]\n                                }, void 0, true, {\n                                    fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                                    lineNumber: 129,\n                                    columnNumber: 19\n                                }, this)\n                            }, void 0, false, {\n                                fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                                lineNumber: 128,\n                                columnNumber: 17\n                            }, this))\n                    }, void 0, false, {\n                        fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                        lineNumber: 124,\n                        columnNumber: 11\n                    }, this),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_13__[\"default\"], {\n                        className: \"mt-3 mb-3 float-right\",\n                        \"aria-label\": \"Pagination\",\n                        children: [\n                            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_10__[\"default\"], {\n                                disabled: page == 1,\n                                onClick: handlePrevious,\n                                size: \"sm\",\n                                variant: \"primary\",\n                                children: \"Previous\"\n                            }, void 0, false, {\n                                fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                                lineNumber: 145,\n                                columnNumber: 11\n                            }, this),\n                            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_barrel_optimize_names_Button_ButtonGroup_Card_Col_Container_Form_Row_Stack_react_bootstrap__WEBPACK_IMPORTED_MODULE_10__[\"default\"], {\n                                disabled: nextpage == 0,\n                                onClick: handleNext,\n                                size: \"sm\",\n                                variant: \"primary\",\n                                children: \"Next\"\n                            }, void 0, false, {\n                                fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                                lineNumber: 146,\n                                columnNumber: 11\n                            }, this)\n                        ]\n                    }, void 0, true, {\n                        fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                        lineNumber: 144,\n                        columnNumber: 9\n                    }, this)\n                ]\n            }, void 0, true, {\n                fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n                lineNumber: 86,\n                columnNumber: 7\n            }, this)\n        ]\n    }, void 0, true, {\n        fileName: \"D:\\\\Work\\\\Tecnichal Test\\\\dans_fe\\\\src\\\\pages\\\\jobs.js\",\n        lineNumber: 84,\n        columnNumber: 5\n    }, this);\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DataList);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvcGFnZXMvam9icy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFtRDtBQUN0QjtBQUNXO0FBRUQ7QUFFTjtBQUU0RDtBQUU3RixTQUFTZTtJQUNQLE1BQU1DLFNBQVNaLHNEQUFTQTtJQUN4QixNQUFNLENBQUNhLFVBQVVDLFlBQVksR0FBR2pCLCtDQUFRQSxDQUFDLEVBQUU7SUFDM0MsTUFBTSxDQUFDa0IsU0FBU0MsV0FBVyxHQUFHbkIsK0NBQVFBLENBQUM7SUFDdkMsTUFBTSxDQUFDb0IsT0FBT0MsU0FBUyxHQUFHckIsK0NBQVFBLENBQUM7SUFDbkMsTUFBTSxDQUFDc0IsTUFBTUMsUUFBUSxHQUFHdkIsK0NBQVFBLENBQUM7SUFDakMsTUFBTSxDQUFDd0IsVUFBVUMsWUFBWSxHQUFHekIsK0NBQVFBLENBQUM7SUFHekMsTUFBTSxDQUFDMEIsTUFBTUMsUUFBUSxHQUFHM0IsK0NBQVFBLENBQUM7SUFDakMsTUFBTSxDQUFDNEIsS0FBS0MsT0FBTyxHQUFHN0IsK0NBQVFBLENBQUM7SUFDL0IsTUFBTSxDQUFDOEIsTUFBTUMsUUFBUSxHQUFHL0IsK0NBQVFBLENBQUM7SUFFakMsTUFBTWdDLG1CQUFtQixDQUFDQyxJQUFNTixRQUFRTSxFQUFFQyxNQUFNLENBQUNDLEtBQUs7SUFDdEQsTUFBTUMsa0JBQWtCLENBQUNILElBQU1KLE9BQU9JLEVBQUVDLE1BQU0sQ0FBQ0MsS0FBSztJQUVwRCxNQUFNRSxhQUFhO1FBQ2pCTixRQUFRLENBQUNEO0lBQ1g7SUFJQTdCLGdEQUFTQSxDQUFDO1FBQ1JxQyxVQUFVLEdBQUcsSUFBSSxJQUFJO0lBQ3ZCLEdBQUcsRUFBRTtJQUVMLE1BQU1BLFlBQVksT0FBT2hCLE1BQU1JLE1BQU1FLEtBQUtFO1FBQ3hDWCxXQUFXO1FBQ1gsSUFBSTtZQUNGLE1BQU1vQixXQUFXQyxhQUFhQyxPQUFPLENBQUM7WUFDdEMsTUFBTUMsVUFBVTtnQkFDZCxpQkFBaUIsQ0FBQyxPQUFPLEVBQUVILFNBQVMsQ0FBQztZQUN2QztZQUNBLE1BQU1JLFdBQVcsTUFBTUMsTUFDckIsQ0FBQyw0Q0FBNEMsRUFBRXRCLEtBQUssYUFBYSxFQUFFSSxLQUFLLFVBQVUsRUFBRUUsSUFBSSxXQUFXLEVBQUVFLEtBQUssQ0FBQyxFQUFFO2dCQUFFWTtZQUFRO1lBSXpILElBQUdDLFNBQVNFLE1BQU0sSUFBSSxLQUFJO2dCQUN4QjlCLE9BQU8rQixJQUFJLENBQUM7WUFDZDtZQUVBLElBQUksQ0FBQ0gsU0FBU0ksRUFBRSxFQUFFO2dCQUNoQixNQUFNLElBQUlDLE1BQU07WUFDbEI7WUFDQSxNQUFNQyxPQUFPLE1BQU1OLFNBQVNPLElBQUk7WUFDaENqQyxZQUFZZ0MsS0FBS0EsSUFBSTtZQUNyQjFCLFFBQVEwQixLQUFLM0IsSUFBSTtZQUNqQkcsWUFBWXdCLEtBQUtFLFFBQVE7UUFFM0IsRUFBRSxPQUFPQyxLQUFLO1lBQ1ovQixTQUFTK0IsSUFBSUMsT0FBTztRQUN0QixTQUFVO1lBQ1JsQyxXQUFXO1FBQ2I7SUFDRjtJQUVBLE1BQU1tQyxhQUFhO1FBQ2pCaEIsVUFBVWQsVUFBVUUsT0FBT0EsT0FBTyxJQUFJRSxNQUFNQSxNQUFNLElBQUlFLE9BQU9BLE9BQU8sS0FBSyx3Q0FBd0M7SUFDbkg7SUFHQSxNQUFNeUIsaUJBQWlCO1FBQ3JCakIsVUFBVWhCLE9BQU8sR0FBR0ksT0FBT0EsT0FBTyxJQUFJRSxNQUFNQSxNQUFNLElBQUlFLE9BQU9BLE9BQU8sS0FBSyx3Q0FBd0M7SUFDbkg7SUFFQSxNQUFNMEIsZUFBZTtRQUNuQmxCLFVBQVVoQixNQUFNSSxPQUFPQSxPQUFPLElBQUlFLE1BQU1BLE1BQU0sSUFBSUUsT0FBT0EsT0FBTyxLQUFLLHdDQUF3QztJQUMvRztJQUlBLHFCQUNFLDhEQUFDMkI7OzBCQUNDLDhEQUFDcEQsaURBQU1BOzs7OzswQkFDUCw4REFBQ0csbUlBQVNBOztrQ0FDUiw4REFBQ0MsbUlBQUdBO3dCQUFDaUQsV0FBVTs7MENBQ2IsOERBQUNoRCxtSUFBR0E7MENBQ0YsNEVBQUNDLG1JQUFJQSxDQUFDZ0QsT0FBTztvQ0FDWEMsTUFBSztvQ0FBT3pCLE9BQU9UO29DQUFNbUMsVUFBVTdCO29DQUFrQjhCLGFBQVk7b0NBQ2pFQyxjQUFXO29DQUNYQyxvQkFBaUI7Ozs7Ozs7Ozs7OzBDQUdyQiw4REFBQ3RELG1JQUFHQTswQ0FDRiw0RUFBQ0MsbUlBQUlBLENBQUNnRCxPQUFPO29DQUNYQyxNQUFLO29DQUFPekIsT0FBT1A7b0NBQUtpQyxVQUFVekI7b0NBQWlCMEIsYUFBWTtvQ0FDL0RDLGNBQVc7b0NBQ1hDLG9CQUFpQjs7Ozs7Ozs7Ozs7MENBR3JCLDhEQUFDdEQsbUlBQUdBOztrREFDRiw4REFBQ0MsbUlBQUlBLENBQUNzRCxLQUFLLENBQUMsa0JBQWtCOzt3Q0FDNUJMLE1BQUs7d0NBQ0xDLFVBQVV4Qjt3Q0FDVjZCLE1BQUs7d0NBQ0xDLFNBQVNyQzt3Q0FDVHNDLElBQUc7d0NBQ0hDLE9BQU07Ozs7OztvQ0FFUHZDOzs7Ozs7OzBDQUVILDhEQUFDcEIsbUlBQUdBOzBDQUNGLDRFQUFDSixvSUFBTUE7b0NBQUNnRSxTQUFTZDtvQ0FBY2UsU0FBUTs4Q0FBa0I7Ozs7Ozs7Ozs7Ozs7Ozs7O29CQUs1RHJELHdCQUNDLDhEQUFDc0Q7a0NBQUU7Ozs7OytCQUNEcEQsc0JBQ0YsOERBQUNvRDs7NEJBQUU7NEJBQVFwRDs7Ozs7OzZDQUVYLDhEQUFDUCxvSUFBS0E7d0JBQUM0RCxLQUFLO3dCQUFHZixXQUFVO2tDQUN0QjFDLFNBQVMwRCxHQUFHLENBQUMsQ0FBQ0MsT0FDYkEsc0JBRUUsOERBQUMvRCxvSUFBSUE7Z0NBQUM4QyxXQUFVOzBDQUNkLDRFQUFDOUMsb0lBQUlBLENBQUNnRSxJQUFJOztzREFDUiw4REFBQ2hFLG9JQUFJQSxDQUFDaUUsS0FBSzs0Q0FBQ25CLFdBQVU7c0RBQWdCaUIsS0FBS0csS0FBSzs7Ozs7O3NEQUNoRCw4REFBQ2xFLG9JQUFJQSxDQUFDbUUsSUFBSTs7Z0RBQ1BKLEtBQUtLLE9BQU87Z0RBQUM7OERBQUcsOERBQUNDO29EQUFFdkIsV0FBVTs4REFBaUNpQixLQUFLZixJQUFJOzs7Ozs7Ozs7Ozs7c0RBRTFFLDhEQUFDMUQsa0RBQUlBOzRDQUFDZ0YsTUFBSzs0Q0FBYUMsSUFBSSxDQUFDLE1BQU0sRUFBRVIsS0FBS1AsRUFBRSxDQUFDLENBQUM7c0RBQzVDLDRFQUFDOUQsb0lBQU1BO2dEQUFDaUUsU0FBUTtnREFBVWEsTUFBSzswREFBSzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2tDQVNsRCw4REFBQzdFLG9JQUFXQTt3QkFBQ21ELFdBQVU7d0JBQXdCSyxjQUFXOzswQ0FDeEQsOERBQUN6RCxvSUFBTUE7Z0NBQUMrRSxVQUFVL0QsUUFBUTtnQ0FBR2dELFNBQVNmO2dDQUFnQjZCLE1BQUs7Z0NBQUtiLFNBQVE7MENBQVU7Ozs7OzswQ0FDbEYsOERBQUNqRSxvSUFBTUE7Z0NBQUMrRSxVQUFVN0QsWUFBWTtnQ0FBRzhDLFNBQVNoQjtnQ0FBWThCLE1BQUs7Z0NBQUtiLFNBQVE7MENBQVU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUs1RjtBQUVBLGlFQUFlekQsUUFBUUEsRUFBQyIsInNvdXJjZXMiOlsid2VicGFjazovL2RhbnNfZmUvLi9zcmMvcGFnZXMvam9icy5qcz8yYTk0Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyB1c2VTdGF0ZSwgdXNlRWZmZWN0IH0gZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgTGluayBmcm9tICduZXh0L2xpbmsnO1xyXG5pbXBvcnQgeyB1c2VSb3V0ZXIgfSBmcm9tICduZXh0L3JvdXRlcic7XHJcblxyXG5pbXBvcnQgUm9vdExheW91dCBmcm9tICcuLi9hcHAvTGF5b3V0JztcclxuXHJcbmltcG9ydCBOYXZiYXIgZnJvbSAnLi4vYXBwL21lbnUnO1xyXG5cclxuaW1wb3J0IHsgQnV0dG9uLCBCdXR0b25Hcm91cCwgQ29udGFpbmVyLCBSb3csIENvbCwgRm9ybSwgQ2FyZCxTdGFjayB9IGZyb20gJ3JlYWN0LWJvb3RzdHJhcCc7XHJcblxyXG5mdW5jdGlvbiBEYXRhTGlzdCgpIHtcclxuICBjb25zdCByb3V0ZXIgPSB1c2VSb3V0ZXIoKTtcclxuICBjb25zdCBbZGF0YUxpc3QsIHNldERhdGFMaXN0XSA9IHVzZVN0YXRlKFtdKTtcclxuICBjb25zdCBbbG9hZGluZywgc2V0TG9hZGluZ10gPSB1c2VTdGF0ZShmYWxzZSk7XHJcbiAgY29uc3QgW2Vycm9yLCBzZXRFcnJvcl0gPSB1c2VTdGF0ZShudWxsKTtcclxuICBjb25zdCBbcGFnZSwgc2V0UGFnZV0gPSB1c2VTdGF0ZSgxKTtcclxuICBjb25zdCBbbmV4dHBhZ2UsIHNldE5leHRwYWdlXSA9IHVzZVN0YXRlKDEpO1xyXG5cclxuXHJcbiAgY29uc3QgW2Rlc2MsIHNldERlc2NdID0gdXNlU3RhdGUoJycpO1xyXG4gIGNvbnN0IFtsb2MsIHNldExvY10gPSB1c2VTdGF0ZSgnJyk7XHJcbiAgY29uc3QgW2Z1bGwsIHNldEZ1bGxdID0gdXNlU3RhdGUoZmFsc2UpO1xyXG5cclxuICBjb25zdCBoYW5kbGVEZXNjQ2hhbmdlID0gKGUpID0+IHNldERlc2MoZS50YXJnZXQudmFsdWUpO1xyXG4gIGNvbnN0IGhhbmRsZUxvY0NoYW5nZSA9IChlKSA9PiBzZXRMb2MoZS50YXJnZXQudmFsdWUpO1xyXG5cclxuICBjb25zdCBoYW5kbGVGdWxsID0gKCkgPT4ge1xyXG4gICAgc2V0RnVsbCghZnVsbCk7XHJcbiAgfTtcclxuXHJcblxyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgZmV0Y2hEYXRhKDEsICcnLCAnJywgJycpO1xyXG4gIH0sIFtdKTtcclxuXHJcbiAgY29uc3QgZmV0Y2hEYXRhID0gYXN5bmMgKHBhZ2UsIGRlc2MsIGxvYywgZnVsbCkgPT4ge1xyXG4gICAgc2V0TG9hZGluZyh0cnVlKTtcclxuICAgIHRyeSB7XHJcbiAgICAgIGNvbnN0IGp3dFRva2VuID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2p3dFRva2VuJyk7XHJcbiAgICAgIGNvbnN0IGhlYWRlcnMgPSB7XHJcbiAgICAgICAgJ0F1dGhvcml6YXRpb24nOiBgQmVhcmVyICR7and0VG9rZW59YFxyXG4gICAgICB9XHJcbiAgICAgIGNvbnN0IHJlc3BvbnNlID0gYXdhaXQgZmV0Y2goXHJcbiAgICAgICAgYGh0dHA6Ly9sb2NhbGhvc3Q6NTAwMC9qb2IvbGlzdD9jdXJyZW50X3BhZ2U9JHtwYWdlfSZkZXNjcmlwdGlvbj0ke2Rlc2N9JmxvY2F0aW9uPSR7bG9jfSZmdWxsX3RpbWU9JHtmdWxsfWAsIHsgaGVhZGVycyB9XHJcblxyXG4gICAgICApO1xyXG4gICAgICBcclxuICAgICAgaWYocmVzcG9uc2Uuc3RhdHVzID09IDQwMSl7XHJcbiAgICAgICAgcm91dGVyLnB1c2goJy9sb2dpbicpO1xyXG4gICAgICB9IFxyXG5cclxuICAgICAgaWYgKCFyZXNwb25zZS5vaykge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcignRmFpbGVkIHRvIGZldGNoIGRhdGEnKTtcclxuICAgICAgfVxyXG4gICAgICBjb25zdCBkYXRhID0gYXdhaXQgcmVzcG9uc2UuanNvbigpO1xyXG4gICAgICBzZXREYXRhTGlzdChkYXRhLmRhdGEpO1xyXG4gICAgICBzZXRQYWdlKGRhdGEucGFnZSlcclxuICAgICAgc2V0TmV4dHBhZ2UoZGF0YS5uZXh0UGFnZSlcclxuXHJcbiAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgc2V0RXJyb3IoZXJyLm1lc3NhZ2UpO1xyXG4gICAgfSBmaW5hbGx5IHtcclxuICAgICAgc2V0TG9hZGluZyhmYWxzZSk7XHJcbiAgICB9XHJcbiAgfTtcclxuXHJcbiAgY29uc3QgaGFuZGxlTmV4dCA9ICgpID0+IHtcclxuICAgIGZldGNoRGF0YShuZXh0cGFnZSwgZGVzYyA/IGRlc2MgOiAnJywgbG9jID8gbG9jIDogJycsIGZ1bGwgPyBmdWxsIDogJycpOyAvLyBGZXRjaCBkYXRhIHdoZW4gdGhlIGJ1dHRvbiBpcyBjbGlja2VkXHJcbiAgfTtcclxuXHJcblxyXG4gIGNvbnN0IGhhbmRsZVByZXZpb3VzID0gKCkgPT4ge1xyXG4gICAgZmV0Y2hEYXRhKHBhZ2UgLSAxLCBkZXNjID8gZGVzYyA6ICcnLCBsb2MgPyBsb2MgOiAnJywgZnVsbCA/IGZ1bGwgOiAnJyk7IC8vIEZldGNoIGRhdGEgd2hlbiB0aGUgYnV0dG9uIGlzIGNsaWNrZWRcclxuICB9O1xyXG5cclxuICBjb25zdCBoYW5kbGVTZWFyY2ggPSAoKSA9PiB7XHJcbiAgICBmZXRjaERhdGEocGFnZSwgZGVzYyA/IGRlc2MgOiAnJywgbG9jID8gbG9jIDogJycsIGZ1bGwgPyBmdWxsIDogJycpOyAvLyBGZXRjaCBkYXRhIHdoZW4gdGhlIGJ1dHRvbiBpcyBjbGlja2VkXHJcbiAgfTtcclxuXHJcblxyXG5cclxuICByZXR1cm4gKFxyXG4gICAgPG1haW4+XHJcbiAgICAgIDxOYXZiYXIgLz5cclxuICAgICAgPENvbnRhaW5lcj5cclxuICAgICAgICA8Um93IGNsYXNzTmFtZT1cIm1iLTNcIj5cclxuICAgICAgICAgIDxDb2w+XHJcbiAgICAgICAgICAgIDxGb3JtLkNvbnRyb2xcclxuICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiIHZhbHVlPXtkZXNjfSBvbkNoYW5nZT17aGFuZGxlRGVzY0NoYW5nZX0gcGxhY2Vob2xkZXI9XCJTZWFyY2ggSm9iIERlc2NyaXB0aW9uXCJcclxuICAgICAgICAgICAgICBhcmlhLWxhYmVsPVwiRGVzY3JpcHRpb25cIlxyXG4gICAgICAgICAgICAgIGFyaWEtZGVzY3JpYmVkYnk9XCJiYXNpYy1hZGRvbjFcIlxyXG4gICAgICAgICAgICAvPlxyXG4gICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgICA8Q29sPlxyXG4gICAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgICAgdHlwZT1cInRleHRcIiB2YWx1ZT17bG9jfSBvbkNoYW5nZT17aGFuZGxlTG9jQ2hhbmdlfSBwbGFjZWhvbGRlcj1cIlNlYXJjaCBKb2IgTG9jYXRpb25cIlxyXG4gICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJEZXNjcmlwdGlvblwiXHJcbiAgICAgICAgICAgICAgYXJpYS1kZXNjcmliZWRieT1cImJhc2ljLWFkZG9uMVwiXHJcbiAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICA8L0NvbD5cclxuICAgICAgICAgIDxDb2w+XHJcbiAgICAgICAgICAgIDxGb3JtLkNoZWNrIC8vIHByZXR0aWVyLWlnbm9yZVxyXG4gICAgICAgICAgICAgIHR5cGU9XCJzd2l0Y2hcIlxyXG4gICAgICAgICAgICAgIG9uQ2hhbmdlPXtoYW5kbGVGdWxsfVxyXG4gICAgICAgICAgICAgIG5hbWU9XCJmdWxsX3RpbWVcIlxyXG4gICAgICAgICAgICAgIGNoZWNrZWQ9e2Z1bGx9XHJcbiAgICAgICAgICAgICAgaWQ9XCJjdXN0b20tc3dpdGNoXCJcclxuICAgICAgICAgICAgICBsYWJlbD1cIkZ1bGwgVGltZSBPbmx5XCJcclxuICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAge2Z1bGx9XHJcbiAgICAgICAgICA8L0NvbD5cclxuICAgICAgICAgIDxDb2w+XHJcbiAgICAgICAgICAgIDxCdXR0b24gb25DbGljaz17aGFuZGxlU2VhcmNofSB2YXJpYW50PVwib3V0bGluZS1wcmltYXJ5XCI+U2VhcmNoPC9CdXR0b24+XHJcbiAgICAgICAgICA8L0NvbD5cclxuICAgICAgICA8L1Jvdz5cclxuXHJcblxyXG4gICAgICAgIHtsb2FkaW5nID8gKFxyXG4gICAgICAgICAgPHA+TG9hZGluZyBkYXRhLi4uPC9wPlxyXG4gICAgICAgICkgOiBlcnJvciA/IChcclxuICAgICAgICAgIDxwPkVycm9yOiB7ZXJyb3J9PC9wPlxyXG4gICAgICAgICkgOiAoXHJcbiAgICAgICAgICA8U3RhY2sgZ2FwPXsyfSBjbGFzc05hbWU9XCJjb2wtbWQtMTIgbXgtYXV0b1wiPlxyXG4gICAgICAgICAgICB7ZGF0YUxpc3QubWFwKChpdGVtKSA9PiAoXHJcbiAgICAgICAgICAgICAgaXRlbSAmJiAoXHJcblxyXG4gICAgICAgICAgICAgICAgPENhcmQgY2xhc3NOYW1lPVwibWItMlwiPlxyXG4gICAgICAgICAgICAgICAgICA8Q2FyZC5Cb2R5PlxyXG4gICAgICAgICAgICAgICAgICAgIDxDYXJkLlRpdGxlIGNsYXNzTmFtZT0ndGV4dC1wcmltYXJ5Jz57aXRlbS50aXRsZX08L0NhcmQuVGl0bGU+XHJcbiAgICAgICAgICAgICAgICAgICAgPENhcmQuVGV4dD5cclxuICAgICAgICAgICAgICAgICAgICAgIHtpdGVtLmNvbXBhbnl9IC0gPGIgY2xhc3NOYW1lPSd0ZXh0LXN1Y2Nlc3MgZm9udC13ZWlnaHQtYm9sZCc+e2l0ZW0udHlwZX08L2I+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9DYXJkLlRleHQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPExpbmsgaHJlZj1cIi9qb2JzL1tpZF1cIiBhcz17YC9qb2JzLyR7aXRlbS5pZH1gfT5cclxuICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gdmFyaWFudD1cInByaW1hcnlcIiBzaXplPVwic21cIj5EZXRhaWw8L0J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICA8L0xpbms+XHJcbiAgICAgICAgICAgICAgICAgIDwvQ2FyZC5Cb2R5PlxyXG4gICAgICAgICAgICAgICAgPC9DYXJkPlxyXG4gICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgKSl9XHJcbiAgICAgICAgICA8L1N0YWNrPlxyXG4gICAgICAgICl9XHJcblxyXG4gICAgICAgIDxCdXR0b25Hcm91cCBjbGFzc05hbWU9XCJtdC0zIG1iLTMgZmxvYXQtcmlnaHRcIiBhcmlhLWxhYmVsPVwiUGFnaW5hdGlvblwiPlxyXG4gICAgICAgICAgPEJ1dHRvbiBkaXNhYmxlZD17cGFnZSA9PSAxfSBvbkNsaWNrPXtoYW5kbGVQcmV2aW91c30gc2l6ZT1cInNtXCIgdmFyaWFudD1cInByaW1hcnlcIj5QcmV2aW91czwvQnV0dG9uPlxyXG4gICAgICAgICAgPEJ1dHRvbiBkaXNhYmxlZD17bmV4dHBhZ2UgPT0gMH0gb25DbGljaz17aGFuZGxlTmV4dH0gc2l6ZT1cInNtXCIgdmFyaWFudD1cInByaW1hcnlcIj5OZXh0PC9CdXR0b24+XHJcbiAgICAgICAgPC9CdXR0b25Hcm91cD5cclxuICAgICAgPC9Db250YWluZXI+XHJcbiAgICA8L21haW4+XHJcbiAgKTtcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgRGF0YUxpc3Q7Il0sIm5hbWVzIjpbIlJlYWN0IiwidXNlU3RhdGUiLCJ1c2VFZmZlY3QiLCJMaW5rIiwidXNlUm91dGVyIiwiUm9vdExheW91dCIsIk5hdmJhciIsIkJ1dHRvbiIsIkJ1dHRvbkdyb3VwIiwiQ29udGFpbmVyIiwiUm93IiwiQ29sIiwiRm9ybSIsIkNhcmQiLCJTdGFjayIsIkRhdGFMaXN0Iiwicm91dGVyIiwiZGF0YUxpc3QiLCJzZXREYXRhTGlzdCIsImxvYWRpbmciLCJzZXRMb2FkaW5nIiwiZXJyb3IiLCJzZXRFcnJvciIsInBhZ2UiLCJzZXRQYWdlIiwibmV4dHBhZ2UiLCJzZXROZXh0cGFnZSIsImRlc2MiLCJzZXREZXNjIiwibG9jIiwic2V0TG9jIiwiZnVsbCIsInNldEZ1bGwiLCJoYW5kbGVEZXNjQ2hhbmdlIiwiZSIsInRhcmdldCIsInZhbHVlIiwiaGFuZGxlTG9jQ2hhbmdlIiwiaGFuZGxlRnVsbCIsImZldGNoRGF0YSIsImp3dFRva2VuIiwibG9jYWxTdG9yYWdlIiwiZ2V0SXRlbSIsImhlYWRlcnMiLCJyZXNwb25zZSIsImZldGNoIiwic3RhdHVzIiwicHVzaCIsIm9rIiwiRXJyb3IiLCJkYXRhIiwianNvbiIsIm5leHRQYWdlIiwiZXJyIiwibWVzc2FnZSIsImhhbmRsZU5leHQiLCJoYW5kbGVQcmV2aW91cyIsImhhbmRsZVNlYXJjaCIsIm1haW4iLCJjbGFzc05hbWUiLCJDb250cm9sIiwidHlwZSIsIm9uQ2hhbmdlIiwicGxhY2Vob2xkZXIiLCJhcmlhLWxhYmVsIiwiYXJpYS1kZXNjcmliZWRieSIsIkNoZWNrIiwibmFtZSIsImNoZWNrZWQiLCJpZCIsImxhYmVsIiwib25DbGljayIsInZhcmlhbnQiLCJwIiwiZ2FwIiwibWFwIiwiaXRlbSIsIkJvZHkiLCJUaXRsZSIsInRpdGxlIiwiVGV4dCIsImNvbXBhbnkiLCJiIiwiaHJlZiIsImFzIiwic2l6ZSIsImRpc2FibGVkIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/pages/jobs.js\n");

/***/ }),

/***/ "./src/app/globals.css":
/*!*****************************!*\
  !*** ./src/app/globals.css ***!
  \*****************************/
/***/ (() => {



/***/ }),

/***/ "@restart/hooks/useBreakpoint":
/*!***********************************************!*\
  !*** external "@restart/hooks/useBreakpoint" ***!
  \***********************************************/
/***/ ((module) => {

"use strict";
module.exports = require("@restart/hooks/useBreakpoint");

/***/ }),

/***/ "@restart/hooks/useEventCallback":
/*!**************************************************!*\
  !*** external "@restart/hooks/useEventCallback" ***!
  \**************************************************/
/***/ ((module) => {

"use strict";
module.exports = require("@restart/hooks/useEventCallback");

/***/ }),

/***/ "@restart/hooks/useMergedRefs":
/*!***********************************************!*\
  !*** external "@restart/hooks/useMergedRefs" ***!
  \***********************************************/
/***/ ((module) => {

"use strict";
module.exports = require("@restart/hooks/useMergedRefs");

/***/ }),

/***/ "@restart/ui/Button":
/*!*************************************!*\
  !*** external "@restart/ui/Button" ***!
  \*************************************/
/***/ ((module) => {

"use strict";
module.exports = require("@restart/ui/Button");

/***/ }),

/***/ "@restart/ui/Modal":
/*!************************************!*\
  !*** external "@restart/ui/Modal" ***!
  \************************************/
/***/ ((module) => {

"use strict";
module.exports = require("@restart/ui/Modal");

/***/ }),

/***/ "@restart/ui/ModalManager":
/*!*******************************************!*\
  !*** external "@restart/ui/ModalManager" ***!
  \*******************************************/
/***/ ((module) => {

"use strict";
module.exports = require("@restart/ui/ModalManager");

/***/ }),

/***/ "@restart/ui/SelectableContext":
/*!************************************************!*\
  !*** external "@restart/ui/SelectableContext" ***!
  \************************************************/
/***/ ((module) => {

"use strict";
module.exports = require("@restart/ui/SelectableContext");

/***/ }),

/***/ "classnames":
/*!*****************************!*\
  !*** external "classnames" ***!
  \*****************************/
/***/ ((module) => {

"use strict";
module.exports = require("classnames");

/***/ }),

/***/ "dom-helpers/addClass":
/*!***************************************!*\
  !*** external "dom-helpers/addClass" ***!
  \***************************************/
/***/ ((module) => {

"use strict";
module.exports = require("dom-helpers/addClass");

/***/ }),

/***/ "dom-helpers/css":
/*!**********************************!*\
  !*** external "dom-helpers/css" ***!
  \**********************************/
/***/ ((module) => {

"use strict";
module.exports = require("dom-helpers/css");

/***/ }),

/***/ "dom-helpers/querySelectorAll":
/*!***********************************************!*\
  !*** external "dom-helpers/querySelectorAll" ***!
  \***********************************************/
/***/ ((module) => {

"use strict";
module.exports = require("dom-helpers/querySelectorAll");

/***/ }),

/***/ "dom-helpers/removeClass":
/*!******************************************!*\
  !*** external "dom-helpers/removeClass" ***!
  \******************************************/
/***/ ((module) => {

"use strict";
module.exports = require("dom-helpers/removeClass");

/***/ }),

/***/ "dom-helpers/transitionEnd":
/*!********************************************!*\
  !*** external "dom-helpers/transitionEnd" ***!
  \********************************************/
/***/ ((module) => {

"use strict";
module.exports = require("dom-helpers/transitionEnd");

/***/ }),

/***/ "next/dist/compiled/next-server/pages.runtime.dev.js":
/*!**********************************************************************!*\
  !*** external "next/dist/compiled/next-server/pages.runtime.dev.js" ***!
  \**********************************************************************/
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/compiled/next-server/pages.runtime.dev.js");

/***/ }),

/***/ "prop-types":
/*!*****************************!*\
  !*** external "prop-types" ***!
  \*****************************/
/***/ ((module) => {

"use strict";
module.exports = require("prop-types");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ "react-dom":
/*!****************************!*\
  !*** external "react-dom" ***!
  \****************************/
/***/ ((module) => {

"use strict";
module.exports = require("react-dom");

/***/ }),

/***/ "react-transition-group/Transition":
/*!****************************************************!*\
  !*** external "react-transition-group/Transition" ***!
  \****************************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react-transition-group/Transition");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "react/jsx-runtime":
/*!************************************!*\
  !*** external "react/jsx-runtime" ***!
  \************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-runtime");

/***/ }),

/***/ "uncontrollable":
/*!*********************************!*\
  !*** external "uncontrollable" ***!
  \*********************************/
/***/ ((module) => {

"use strict";
module.exports = require("uncontrollable");

/***/ }),

/***/ "warning":
/*!**************************!*\
  !*** external "warning" ***!
  \**************************/
/***/ ((module) => {

"use strict";
module.exports = require("warning");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/***/ ((module) => {

"use strict";
module.exports = require("fs");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/***/ ((module) => {

"use strict";
module.exports = require("path");

/***/ }),

/***/ "stream":
/*!*************************!*\
  !*** external "stream" ***!
  \*************************/
/***/ ((module) => {

"use strict";
module.exports = require("stream");

/***/ }),

/***/ "zlib":
/*!***********************!*\
  !*** external "zlib" ***!
  \***********************/
/***/ ((module) => {

"use strict";
module.exports = require("zlib");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, ["vendor-chunks/next","vendor-chunks/@swc","vendor-chunks/react-bootstrap","vendor-chunks/bootstrap"], () => (__webpack_exec__("./node_modules/next/dist/build/webpack/loaders/next-route-loader/index.js?kind=PAGES&page=%2Fjobs&preferredRegion=&absolutePagePath=.%2Fsrc%5Cpages%5Cjobs.js&absoluteAppPath=private-next-pages%2F_app&absoluteDocumentPath=private-next-pages%2F_document&middlewareConfigBase64=e30%3D!")));
module.exports = __webpack_exports__;

})();